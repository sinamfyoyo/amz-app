import http from './interface'
//设置请求前拦截器
// http.interceptor.request = (config) => {
// 	config.header = {
// 		"token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
// 	}
// } 
export const getNavbar = (data) => {
	//设置请求结束后拦截器
	http.interceptor.response = (response) => {
		if (response.data.errCode == '200') {
			return response.data;
		} else return false;
	}
	return http.request({
		url: 'xcxapi-getNavbarData?apiV=2',
	})
	
}
export const getNavData= (data) => {
	//设置请求结束后拦截器
	http.interceptor.response = (response) => {
		if (response.data.errCode == '200') {
			return response.data;
		} else return false;
	}
	return http.request({
		data:data,
		url: 'xcxapi-getForumData?apiV=2',
	})
	
}
export default {
	getNavbar,getNavData
}
